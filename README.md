# Coding challenge

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

`npm install` to install the dependencies 

`npm start` to start the project


## Description of the solution

This app was create based on the mockup presented in [this link](https://todomvc.com/examples/vanillajs/#/), and following the instructions presented in [this note page](https://geraldtech.notion.site/Frontend-Developer-099cd139d3e64038978707f98e64f356).

There are a few ways to structure this project but I did my best with the amount of time available. 

## Future refactor

- Styles to make it more similar to the mockup.
- Add a functionality to select all todos
- Add a functionality to remove the selected ones (more than one)
- Add test using rtl for unit test and some cypress as UJT.
- Auto re-build (now you need to reload the app to see the changes)
- remove unused files.


## What this app can do?

This app can record your todos and also have some of them stored in the code to be used as example. 

You can:
- Create new todos
- Remove a todo
- See how many todos are left

