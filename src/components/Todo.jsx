import React, { useState, useEffect } from 'react';
import './Todo.css';

const CreateTask = ({ addTask }) => {
  const [value, setValue] = useState("");

  const handleSubmit = e => {
    e.preventDefault();
    if (!value) return;

    addTask(value);
    setValue("");
  }

  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        className="input"
        value={value}
        placeholder="What needs to be done?"
        onChange={e => setValue(e.target.value)}
      />
    </form>
  );
}

const Task = ({ task, index, completeTask, removeTask }) => {
  const [checked, setChecked] = useState(task.completed);

  const handleChange = () => {
    setChecked(!checked && completeTask(index));
  };

  return (
    <div className='task-content'>
      <label>
        <input
          type="checkbox"
          style={{ textDecoration: task.completed ? "line-through" : "" }}
          checked={checked}
          onChange={handleChange}
          className='completed-task'
        />
        {task.title}
      </label>
      <button className='remove-task' onClick={() => removeTask(index)}>x</button>
    </div>
  );
}


const Todo = () => {
  const [tasksRemaining, setTasksRemaining] = useState(0);
  const [tasks, setTasks] = useState([
    {
      title: "Study",
      completed: true
    },
    {
      title: "Rollerblades Workout",
      completed: false
    },
    {
      title: "Call mom",
      completed: false
    }
  ]);

  useEffect(() => {
    setTasksRemaining(tasks.filter(task => !task.completed).length)
  });

  const addTask = title => {
    const newTasks = [...tasks, { title, completed: false }];
    setTasks(newTasks);
  };

  const completeTask = index => {
    const newTasks = [...tasks];
    newTasks[index].completed = true;
    setTasks(newTasks);
  };

  const removeTask = index => {
    const newTasks = [...tasks];
    newTasks.splice(index, 1);
    setTasks(newTasks);
  };

  return (
    <div>
      <h1 className='todo-title'>todos</h1>
      <div className="todo-container">
        <div className="create-task" >
          <CreateTask addTask={addTask} />
        </div>
        <div className="tasks">
          {tasks.map((task, index) => (
            <Task
              task={task}
              index={index}
              completeTask={completeTask}
              removeTask={removeTask}
              key={index}
            />
          ))}
        </div>
        <div className="remaining-tasks">
          {`${tasksRemaining} item${tasksRemaining >1 ? "s": ""} left`}</div>
      </div>

    </div>
  );
}

export default Todo;